{-# LANGUAGE DeriveFunctor    #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators    #-}
module
  Parser.Math
  ( Math
  , plus
  , minus
  , times
  , div
  , mod
  , neg
  )
where

import           Control.Monad
import           Control.Monad.Free
import           Data.Functor.Foldable
import           Data.Semigroup
import           Prelude               hiding (div, mod)

import           Parser.Generic
import           Parser.PrintExpr
import           TypeCheck.Infer

data MathBinOp = Plus | Minus | Times | Div | Mod

opSymbol :: MathBinOp -> String
opSymbol Plus  = " + "
opSymbol Minus = " - "
opSymbol Times = " * "
opSymbol Div   = " / "
opSymbol Mod   = " % "

data Math a
  = BinOp (InfixOp MathBinOp a)
  | Negate a
    deriving (Functor)

instance (PrintExpr a) => PrintExpr (Math a) where
  printExpr (BinOp op) = foldMapInfix opSymbol printExpr op
  printExpr (Negate x) = "-" <> printExpr x

freeBinOp :: (Inject Math f, MonadFree f m) => MathBinOp -> m t -> m t -> m t
freeBinOp op x y = wrap . inject $ BinOp (InfixOp op x y)

plus :: (Inject Math f, MonadFree f m) => m t -> m t -> m t
plus = freeBinOp Plus

minus :: (Inject Math f, MonadFree f m) => m t -> m t -> m t
minus = freeBinOp Minus

times :: (Inject Math f, MonadFree f m) => m t -> m t -> m t
times = freeBinOp Times

div :: (Inject Math f, MonadFree f m) => m t -> m t -> m t
div = freeBinOp Div

mod :: (Inject Math f, MonadFree f m) => m t -> m t -> m t
mod = freeBinOp Mod

neg :: (Inject Math f, MonadFree f m) => m t -> m t
neg x = wrap . inject $ Negate x
