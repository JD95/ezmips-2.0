{-# LANGUAGE DeriveFunctor    #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators    #-}
module
  Parser.Prim
  ( Prim
  , sym
  , int
  )
where

import           Control.Monad.Free

import           Parser.Generic
import           Parser.PrintExpr

data Prim a
  = Sym String
  | Int Int
    deriving (Functor)

instance PrintExpr (Prim a) where
  printExpr (Sym n) = n
  printExpr (Int i) = show i

sym :: (Inject Prim f, MonadFree f m) => String -> m a
sym = wrap . inject . Sym

int :: (Inject Prim f, MonadFree f m) => Int -> m a
int = wrap . inject . Int
