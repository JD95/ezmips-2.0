{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators     #-}
module Parser.PrintExpr where

class PrintExpr exp where
  printExpr :: exp -> String

instance PrintExpr [Char] where
  printExpr = id
