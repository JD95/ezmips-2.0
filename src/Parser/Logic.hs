module Parser.Logic (Logic) where

import           Prelude          (String)
import           Protolude        hiding (EQ, GT, LT)

import           Parser.Generic
import           Parser.PrintExpr

data LogicBinOp = And | Or | LT | EQ | GT

opSymbol :: LogicBinOp -> String
opSymbol And = " && "
opSymbol Or  = " || "
opSymbol LT  = " < "
opSymbol EQ  = " == "
opSymbol GT  = " > "

data Logic a = BinOp (InfixOp LogicBinOp a) | Not a

instance (PrintExpr a) => PrintExpr (Logic a) where
  printExpr (BinOp op) = foldMapInfix opSymbol printExpr op
  printExpr (Not x)    = "!" <> printExpr x

