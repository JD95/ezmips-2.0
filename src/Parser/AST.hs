{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators    #-}
module Parser.AST where

import           Control.Monad.Free
import qualified Control.Monad.Trans.Free as F
import qualified Data.ByteString.Lazy     as LBS
import           Data.Functor.Foldable
import qualified Data.Text                as T
import qualified Prelude

import           Parser.Function
import           Parser.Generic
import           Parser.Logic
import           Parser.Math
import           Parser.Prim
import           Parser.PrintExpr
import           Parser.Statement

type Name = LBS.ByteString
type VType = LBS.ByteString

type Exp_ = Prim :+: Math

type Exp = Fix Exp_

instance (PrintExpr a, PrintExpr (f a), PrintExpr (g a)) =>
  PrintExpr ((f :+: g) a) where
  printExpr (L x) = printExpr x
  printExpr (R x) = printExpr x

testExpr :: Free Exp_ a
testExpr = int 1 `plus` int 1

test = iter printExpr testExpr
