module Parser.Statement where

data Stmt a
  = Action a
  | If a [a] (Maybe a)
  | While a [a]
  | Decl a a
  | Assign a a

