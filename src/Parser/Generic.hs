{-# LANGUAGE DeriveFunctor          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE UndecidableInstances   #-}

module Parser.Generic where

import TypeCheck.Infer
import           Data.Semigroup

data InfixOp k a = InfixOp k a a deriving (Functor)

foldMapInfix :: Semigroup b => (k -> b) -> (a -> b) -> InfixOp k a -> b
foldMapInfix f g (InfixOp k x y) = (g x) <> (f k) <> (g y)

data (:+:) (a :: * -> *) (b :: * -> *) c
  = L (a c) | R (b c)

instance (Functor f, Functor g) => Functor (f :+: g) where
  fmap f (L x) = L (fmap f x)
  fmap f (R x) = R (fmap f x)

class Inject f fs where
  inject :: f a -> fs a

instance Inject f f where
  inject = id

instance Inject f (f :+: g) where
  inject x = L x

instance Inject f (g :+: f) where
  inject x = R x

instance (Inject f (h :+: i)) => Inject f (g :+: (h :+: i)) where
  inject x = R (inject x)

