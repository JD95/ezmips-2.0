{-# LANGUAGE DeriveFunctor    #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies     #-}

module TypeCheck.Inference where

import           Control.Comonad.Cofree
import           Control.Monad.Identity
import           Data.Semigroup

data IError t
  = WrongType t t
  | InvalidFuncArgs [t] [t]
  | UndefinedSymbol String
  | RedeclaredVar String
  | InvalidType String
  | BadVariableDecl
  | InvalidAssign t t
  | MustAssignToLValue
  | Errors [IError t]
    deriving (Show, Eq, Functor)

instance Semigroup (IError t) where
  (Errors xs) <> (Errors ys) = Errors (xs <> ys)
  (Errors xs) <> e = Errors (xs <> [e])
  e <> (Errors xs) = Errors (e:xs)
  x <> y = Errors [x,y]

type Inference exp t = Either (IError t) (Cofree exp t)
