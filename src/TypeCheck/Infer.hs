{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}

module
  TypeCheck.Infer
  ( IType
  , TypeInference
  , module TypeCheck.Inference
  )
where

import           Control.Comonad
import           Control.Comonad.Cofree
import           Control.Monad.Reader
import           Data.Bifunctor
import           Data.Functor.Foldable
import qualified Data.Map               as Map
import           Data.Semigroup

import           TypeCheck.Inference

newtype Name = Name String deriving (Show, Eq)
newtype VarDecl = VarDecl String deriving (Show, Eq)

data IType
  = IInt
  | IVoid
  | ISym Name
  | IDecl IType VarDecl
  | IBool
  | ITypeTag IType
  | IFunc IType [IType]
    deriving (Prelude.Show, Prelude.Eq)

newtype TypeInference exp a
  = TypeInference
  { runInference :: Reader (Map.Map Name a) (Cofree exp (Either (IError a) a)) }

typeCheck
  :: Traversable exp
  => (Map.Map Name a)
  -> TypeInference exp a
  -> Either (IError a) (Cofree exp a)
typeCheck gamma = traverse id . flip runReader gamma . runInference

class InferType exp where
  infer :: exp IType -> TypeInference exp IType

match :: (Functor exp, Eq t) => t -> Cofree exp t -> Inference exp t
match x y =
  if x == extract y
    then Right y
    else Left (WrongType x (extract y))

foldResults :: [Inference exp t] -> Either (IError t) [Cofree exp t]
foldResults = foldr f (Right [])
  where f (Left x) (Left y)    = Left (x <> y)
        f (Left x) _           = Left x
        f _ (Left y)           = Left y
        f (Right x) (Right xs) = Right (x:xs)

matchAll
  :: (Traversable exp, Eq t)
  => [t]
  -> [Cofree exp t]
  -> Either (IError t) [Cofree exp t]
matchAll toMatch xs = foldResults $ zipWith match toMatch xs
